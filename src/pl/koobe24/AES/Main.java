package pl.koobe24.AES;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;

public class Main {

    private static final int KEY_SIZE = 16;
    private static final int IV_SIZE = 16;

    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println(args.length);
            help();
            return;
        }
        if (!args[0].equals("dec") && !args[0].equals("enc")) {
            help();
        }

        File inputFile = new File(args[1]);
        File outputFile = new File(args[2]);
        if (args[0].equals("enc") && args.length > 4) {
            int c;
            StringBuilder builder = new StringBuilder();
            try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(inputFile))) {
                while ((c = input.read()) != -1) {
                    builder.append((char) c);
                }
            } catch (FileNotFoundException e) {
                System.err.println("Nie znaleziono pliku wejsciowego");
                return;
            } catch (IOException e) {
                System.err.println("IOException");
                return;
            }

            try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(outputFile))) {
                byte[] encrypted = encrypt(builder.toString(), args[3], args[4]);
                output.write(encrypted);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (args[0].equals("dec")) {
            byte[] buffer;
            try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(inputFile));
                 BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(outputFile))) {
                buffer = new byte[input.available()];
                input.read(buffer);
                String decrypted = decrypt(buffer, args[3]);
                output.write(decrypted.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            help();
        }


    }

    private static void help() {
        System.out.println("Sposob uzycia: java -jar AES.jar enc|dec <plik wejsciowy> <plik wyjsciowy> <plik klucza> <zrodlo losowosci> (dla szyfrowania)");
        System.out.println("Przyklad szyfrowanie: java -jar AES.jar enc wejsciowy.txt wyjsciowy.txt klucz.txt /dev/random");
        System.out.println("Przyklad odszyfrowywanie: java -jar AES.jar dec zaszyfrowane.txt odszyfrowane.txt klucz.txt");
    }

    private static byte[] encrypt(String text, String keyFileName, String entropySource) throws Exception {
        byte[] keyBuf = new byte[KEY_SIZE];
        byte[] ivBuf = new byte[IV_SIZE];
        File file = new File(entropySource);
        File keyFile = new File(keyFileName);
        try (BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
             BufferedOutputStream bufOut = new BufferedOutputStream(new FileOutputStream(keyFile))) {
            buf.read(keyBuf, 0, keyBuf.length);
            buf.read(ivBuf, 0, ivBuf.length);

            bufOut.write(keyBuf, 0, keyBuf.length);
            bufOut.write(ivBuf, 0, ivBuf.length);
        } catch (FileNotFoundException e) {
            System.err.println("Nie znaleziono pliku random");
        } catch (IOException e) {
            System.err.println("IOException");
        }

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        SecretKeySpec keySpec = new SecretKeySpec(keyBuf, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(ivBuf));
        return cipher.doFinal(text.getBytes());
    }

    private static String decrypt(byte[] cipherText, String keyFileName) throws Exception {
        File keyFile = new File(keyFileName);
        byte[] keyBuf = new byte[KEY_SIZE];
        byte[] ivBuf = new byte[IV_SIZE];
        try (BufferedInputStream buf = new BufferedInputStream(new FileInputStream(keyFile))) {
            buf.read(keyBuf, 0, keyBuf.length);
            buf.read(ivBuf, 0, ivBuf.length);
        }
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        SecretKeySpec keySpec = new SecretKeySpec(keyBuf, "AES");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(ivBuf));
        return new String(cipher.doFinal(cipherText));
    }
}
